const Post = require('../models/post');

const slugify = require('slugify');
const { findOne } = require('../models/post');
exports.create = (req, res) => {

    // console.log(req.body);

    // def d'un ensemble de variables 

    // by creating theses variables we take from req.body the title, content and user
    const { title, content, user } = req.body // au lieu de mettre const title = { title : req.body.title}...
    const slug = slugify(title) // if the title is <My Post> it'll be transformed to <my-post>

    // validate
    if (!title || !content) {

        res.status(400).json({
            error: 'Title and content is required ! '
        })
    }

    // res.json({
    //     message : 'Take a look at your server console !'
    // })
    //__________________________________________HERE______________________________________________
    // create a post request
    // the create method takes two arguments and the second one is a callback function

    Post.create({ title, content, user, slug }, (err, post) => {  // here if key and value are the same we can just give the key -> title : title
        // when u try to create a new post- eather it'll be successfull or there will be an error

        if (err) {
            console.log(err)
            res.status(400).json({ error: 'Try another title !' })
        }


        res.json(post);


    })




};

exports.list = async (req, res) => {
    let post = await Post.find({}) // .limit(10).sort({ createdAt: -1 }) // waiting for response to get the data from the db //sort({createdAt : -1}) makes the last data displayed
    if (!post) return res.status(400).json({ message: "error" })
    console.log('yaya', post)  // return error msg to the client
    return res.status(200).json(post)  // return the res to the client
}

/* Bad way to do the same thing */
/*
exports.list = (req,res) =>{
        Post.find({}).exec(err,getpost)=>{
            if(err) console.log(err)
            res.json(getpost)
        }

};

*/

/* with list method we were able to find all the posts 
this time we want to find only one post bases to it's slug as id -> '/post/:slug'
 */

// exports.read = (req,res) =>{
//     //getting the slug from the req.params
//     const {slug} = req.params
//     // getting the interest post with findOne
//     Post.findOne({slug}).exec(err,onepost)=>{
//         if(err) console.log(err)
//         res.json(onepost)
//     };

// };


exports.read = async (req, res) => {
    const value = req.params.abdou // req.params.elt -> pour accéder à l'élément d'url /:elt 
    console.log(value)
    const ax = await Post.findOne({ slug: value })  // using slug here because in the front we use a slug also to get to this value // slug is a key in the db used to fetch data
    if (!ax) return res.status(400).json({ message: 'error 😒' })
    return res.status(200).json(ax)
}

exports.update = async (req, res) => {
    // define the value
    const val = req.params.abdou
    // pour faire une modification un titre par exemple il faut l'ancine elt et le comparer au nouveau
    // on prend l'ancien de req.body _> 
    const { title, content, user } = req.body               //
    console.log('nono', req.body)
    const uppy = await Post.findOneAndUpdate({ slug: val }, { title, content, user }, { new: true }) // first argument path based on the slug, second argument is the actual content that we want to update,third argument to be sure to return post with updated content, the new
    if (!uppy) return res.status(400).json({ message: 'error while updating data 😒' })
    return res.status(200).json(uppy);

}


exports.remove = async (req, res) => {
    const value = req.params.abdou // req.params.elt -> pour accéder à l'élément d'url /:elt 
    const rm = await Post.findOneAndRemove({ slug: value })  // using slug here because in the front we use a slug also to get to this value
    if (!rm) return res.status(400).json({ message: 'error while removing data 😒' })
    return res.status(200).json({ message: 'Delete done 🔥 ' })
}

