const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt'); // package used to validate the jwt token

/* as long as the token is valid user'll able to login */

exports.login = (req, res) => {
    const { name, password } = req.body;
    if (password === process.env.PASSWORD) { // now we are sure that we have the correct user
        // generate token and send to client/react
        const token = jwt.sign({ name }, process.env.JWT_SECRET, { expiresIn: '1d' }); // first argument : { name } , second argument : process.env.JWT_SECRET , third argument axpiray date :  { expiresIn: '1d' }
        return res.json({ token, name }); // we send the name also in order to use it in our client side application
    } else {
        return res.status(400).json({
            error: 'Incorrect password!'
        })
    }
};


