const express = require('express');
const morgan = require('morgan')
const bodyParser = require('body-parser');
require('mongodb')
const cors = require('cors')
const mongoose = require('mongoose');
require('dotenv').config();



// import routes
const postRoutes = require('./routes/post');
const authRoutes = require('./routes/auth');


// app
const app = express();
app.use(express.json()) //  It parses incoming JSON requests and puts the parsed data in req without this your requests will not work ! 


// db
mongoose
    .connect(process.env.DATABASE, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    })
    .then(() => console.log('DB connected'))
    .catch(err => console.log(err));

// middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());

// route middleware
app.use('/api', postRoutes);
app.use('/api', authRoutes);


// port
const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Server is running on port ${port}`));

