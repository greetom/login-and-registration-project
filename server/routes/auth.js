const express = require('express');

const router = express.Router();

// import controller methods
const { login} = require('../controllers/auth');

// create,list,read  these are methods in controllers/post file and every method'll be applied to get or post something through our url

router.post('/login',login); // this is a post method to save the user info




module.exports = router;
