const express = require('express');
const router = express.Router();
// import controller methods
const { create, list, read, update, remove } = require('../controllers/post');
// create,list,read  these are methods in controllers/post file and every method'll be applied to get or post something through our url
router.post('/post', create); // this is a post method to create a new post
router.get('/getpost', list)
router.get('/post/:abdou', read) // this is accessible using req.params.slug // read -> to read a single post

/* Let's add to more routes one for update and one for delete */
router.put('/post/:abdou', update)  // route for update
router.delete('/post/:abdou', remove)
module.exports = router;

