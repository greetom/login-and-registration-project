// const express = require('express');
// app.use(express.json());
// const dotenv = require('dotenv')

// // import du router dans mon application

// const stuffRouter = require('./routes/stuff')

// // pour rendre l'import effectif

// app.use('/api/stuff',stuffRouter)




// /* ici on importe mongoose pour pouvoir importer le model qu'on vient de créer et l'utiliser pour
//  effectuer des opération dans notre base de donnée */

//  const mongoose = require('mongoose');

// const { response } = require('express');

//  mongoose.connect(process.env.DATABASE, {useNewUrlParser : true,
//     useUnifiedTopology : true})
//     .then(() =>{
//         console.log('Connected to the database !')
//     } )
//     .catch((err) =>{
//         console.log(err)
//     } )


//     const app = express()

//     // // Enregister dans la base 

//     // // ici on via notre requête post on défini ce qu'on va mettre dans la base de donnée et pour cela on a un modèle qui décrit la structure des informations qu'on passe à la bd
//     // app.post('/api/stuff',(req,res,next) =>{

//     //     // va falloir supprimer le id avant

//     //     delete req.body._id; // c'est l'id envoyé par le front

//     //     /* tout ce qui est posté va se retrouver dans le corps de la requête
//     //      on va donc utiliser le corps de cette requête pour remplir notre modèle Thing
//     //     */ 
//     //    const Thing = new Thing({

//     //     /* au lieu de faire title : req.body.title, 
//     //                         etc,
//     //                         etc..
           
//     //                         on va aller copier les champs qu'il y a dans le corps de la request
//     //     */
//     //     ...req.body

//     //    })

//     //            // on enregistre l'objet dans la base à présent
//     //    Thing.save()
//     //    .then(() =>{
//     //       res.status(200).json({ message : 'Enregistrement réussi' })
//     //    } )
//     //    .catch((err) => res.status(400).json({err}) ) // err : err = err ici 


//     // } )
//     //     // Lire dans la base *

//     //     // modifer avec route put 
//     //     /* dans le nouvel objet on utilise (...req.body) pour récuperer le thing qui est dans le corps de la requête
//     //      puis on vient dire que l'id correspond à celui des paramètres 
//     //      car celui qui est dans le corps de la requête ne sera pas forcément le bon */


//     //      // Avec put l'utilisateur va envoyer un nouvel abjet

//     //     app.put('/api/stuff/:id',(req,res,next) =>{
//     //             Thing.updateOne({_id : req.params.id }, {...req.body, _id : req.params.id})
//     //             .then(() =>{
//     //                 res.status(200).json({message : 'Objet modifié !'})
//     //             })
//     //             .catch(err =>{
//     //                 res.status(400).json({err})
//     //             })

//     //     })

//     //     app.delete('/api/stuff/:id', (req,res,next) => {
//     //         Thing.deleteOne({_id : req.params.id})
//     //         .then(() =>
//     //             res.status(200).json({message : 'Suppression terminée ! '})
//     //         )
//     //         .catch(err => res.status(400).json({err}))
//     //     })


//     //     // trouver un objet specifique

//     //     app.get('/api/stuff/:id',(req,res,next) =>{ // (:id) cette partie de la route est dynamique et j'y autrai accès dans req.params.id
//     //         Thing.findOne({_id : req.params.id}) // on veut que l'id de l'objet qu'on veut (_id) soit le même que celui qui se trouve dans le paramètre de requête
//     //         .then( thing =>{
//     //             res.status(200).json(thing)
//     //         })
//     //         .then( err =>{
//     //             res.status(400).json({err})
//     //         })

//     //     })


//     //      // En plus d'enregister des choses dans la base, notre modèle Thing qui appartient à la collection Things permets aussi de lire dans la base

//     //      app.get('/api/stuff',(req,res,next) =>{
//     //             // on va utiliser find pour récuperer la collection Things qui existe dans la bd
//     //             Thing.find()
//     //             .then((Things) =>{
//     //                 res.status(200).json(Things)
//     //             } )
//     //             .catch((err) =>{
//     //                 res.status(400).json({err})
//     //             } )
//     //      } )




//          module.exports = app

       