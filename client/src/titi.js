// import React,{useState} from 'react';
// import axios from 'axios'


// const Create = () => {
//   // state
//   const [state,setState] = useState({ //  state is the informations that user files from the browser and setState is the method to update the state
//     title :"",
//     content:"",
//     user:""
//   })
//   // destructure values from state
  
//   const {title,content,user} = state; // la veleur (value) inserée dans les input sera égale à par ex title = state.title donc stocké dans state pareil pour content et user
  
//   const hendleSubmit = (e) => {
//     e.preventDefault(); // usually when we hit the sumbit button the page reloads here we prevent that default behavior
//     // console.table({title,content,user})
//     axios.post(`${process.env.REACT_APP_API}/post`,{title,content,user}) // first argument -> url , second one the data that we are sending
//     .then((res) =>{
//     // empty the state -> anything we want to do with the state we use setState method
//     setState(...state, {title:'',content:'',user:""})

//       // show success alert
//       alert(`Post titled ${res.data.title} is created`)
       
       
//     })
//     .catch((error) =>{
//           console.log(error.res)
//           alert(error.res.data.res)
//     })
//   }
//   // onchange event handler 
  
//   /* hendleChange ce qui se passe quand l'utilisateur click sur tab pour changer de champ ou en sort */
  
//   const hendleChange = (name) => (event) =>{ 
//     // updating state informations now...
//     // (name) is the first argument (hendleChange('title'),hendleChange('Content'),hendleChange('user')) and here it is updated using the event
//     // event.target.value c'est ce que l'utilisateur entre dans l'input
//     setState(...state, {[name] : event.target.value})  // setState qui va appliquer hendleChange à ...state ici
//   } 
  
//   // Another one !!!!!!!!!!!!!!!!!!!!!!!!!
  
//   /* funtion hendleChange(name){
//     return function(event){
//       setState(...state, {[name] : event.target.value}) // hendleChange est une fonction qui va prendre name et lui donner la valeur de ce que l'utilisateur à rentré cad event.target.value
//     }
//   }
//   */
 
//  return ( // we want to crontrol the values in our input fields based to what we have on the state , the updated state will be the value bellow 
//  <div className='container p-5' >
//       <h1>CREATE POST</h1>
//       <br/>
//       <form onSubmit={hendleSubmit}>    
//               <div className='from-group'>
//                   <label className='text-muted' >Title</label>
      
//                   <input onchange = {hendleChange('title')} value={title} type='text' className='form-control'  placeholder='Give title' required/>
//               </div>
//               <div className='from-group'>
//                   <label className='text-muted' >Content</label>
//                   <textarea onchange = {hendleChange('Content')} value={content}  type='text' className='form-control'   placeholder='Write something...' required/>
//               </div>
//               <div className='from-group'>
//                   <label className='text-muted' >User</label>
      
//                   <input  onchange = {hendleChange('user')} value={user}  type='text' className='form-control'  placeholder='Enter user name' required/>
//               </div>
//               <div> 
//                 <button className="buttton.btn.btn-primary">Create</button>
//               </div>
//       </form>
//     </div>
//   );
  

// }
// export default Create;
// //

   // this is the state
//    const [posts,setPosts] = useState([])

//    // when the component mounts -> gettings all requests from our server and populate in state
  
//    const fetchPosts = () =>{   // fetch = aller chercher    
 
//      // now getting all the posts from our server
//      axios.get(`${process.env.REACT_APP_API}/getpost`)
//      .then((res) =>{console.log(res)
//        setPosts(res.data)
//      })
//      .catch((err) => alert('Error Fetching posts'))
 
//  } 
//  let fetchPosts = () =>{
//   return( axios.get(`${process.env.REACT_APP_API}/getpost`)
//   .then((res)=>{
//     console.log(res)
//     setPosts(res.data)
//   })
//   .catch((err)=> alert('Error Fetching posts'))
// )
 

//________________________________________________________________________    ______________________________________________________________________________________________






