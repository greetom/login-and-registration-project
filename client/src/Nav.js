import React from "react";
import { Link, withRouter } from "react-router-dom";
import {getUser,logout } from "./helpers";

 const Nav = ({history}) =>(
 <nav>
    <ul className="nav nav-tabs" >
        <li className ='nav-item pr-3 pt-3 pb-3'>
            <Link to='/'>Home</Link>
        </li>
        <li className =' nav-item pr-3 pt-3 pb-3'>
            <Link to="/create">Create</Link>              
        </li>

        {!getUser() && ( 
             <li  className =' nav-item ml-auto pr-3 pt-3 pb-3' >        {/* getUser is a function that either returns the user to false _> !getUser means if we do not have the user _> show the Login */}
             <Link to="/login">Login</Link>
            </li>)}     

              {/* All this code in between is to show the login conditionnaly */}

        {getUser() && (
            <li 
            onClick={() => logout(()=>history.push('/'))}   
            className =' nav-item ml-auto pr-3 pt-3 pb-3'
                style={{cursor : 'pointer'}}
                >  {/* executting logout function whenener logout button hitted we redirect userto the homme page getUser means if we have the user _> show the logout */}
                logout         
            </li>
          )}



    </ul>
</nav>
)
   
    
    // 
 // 

 export default withRouter(Nav);