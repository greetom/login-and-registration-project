/*this file'll help us save the response in the session storage*/

// save login response > (user's name and token ) to session storage // so authenticate method will help us save the response from axios

export const authenticate = (response,next) =>{ 
    if(window!== 'undifined'){ //not undefined _> means that we have the window object
        console.log(authenticate,response)
        sessionStorage.setItem('token',JSON.stringify(response.data.token)) // setItem allows us to save things in the session storage 
        sessionStorage.setItem('user',JSON.stringify(response.data.name))  // JSON.stringify _> to save it as Json file from javascript object
 
    }
    next(); // this callback function allows us to redirect the user right after that
}

// access token name from the session storage // informations are set now _> we want to retrieve that data

export const getToken = (response,next) =>{
    if(window!== 'undifined'){
        if(sessionStorage.getItem('token')){ // making sure that the item by the name of token is in the session storage
            return JSON.parse(sessionStorage.getItem('token')) // retruning that token as a javascript object from JSON file 
        } else {
            return false;
        }
       
    }
};



// access user from the session storage

export const getUser = () =>{
    if(window!== 'undifined'){
        if(sessionStorage.getItem('user')){
            return sessionStorage.getItem('user')
        } else {
            return false;
        }
       
    }
};

// remove token and userfrom session storage _> whenever he hits logout button

export const logout = next =>{
    if(window !== 'undifined'){
        // console.log(authenticate,response)
        sessionStorage.removeItem('token')
        sessionStorage.removeItem('user')

    }
    next()
};