import React from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom' //intead of importing react-router-dom we import the some packages from it
import App from './App'
import SinglePost from './SinglePost';
import UpdatePost from './UpdatePost';
import Login from './Login';
                                    /* Here we have Routes which is the routing system which will show different components based on the path*/
// Every page correponds to a different route and a component will be applided to that route

/*we are going to wrap all our react components (every component is applied to a route)
 inside the BrowserRouter component so that certain properties will be available in those packages(components)*/


 // which path our route hendles , excat component makes sur we get the right is applied to our route 

 // Switch component help us switch between different component 

 // Routes is the routing system which will show diff components based on the path

 // state is an object that includes any data this component needs
 import Create from './Create'
 const Routes = function(){

    return(<BrowserRouter>   

    
            <Switch>

            <Route path = '/' exact  component ={App}  />  
    
            <Route path = '/create' exact component ={Create} />  

            <Route path = '/login' exact  component ={Login}  />  


            <Route path = '/post/:abdou' exact component ={SinglePost} /> 

            <Route path = '/post/update/:abdou' exact component ={UpdatePost} /> 


            </Switch>
    
    
    </BrowserRouter>)
    
 }

 export default Routes