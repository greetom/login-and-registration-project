import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link, withRouter } from 'react-router-dom' // withRouter helps to access the history when we wrap our component with it
import Nav from './Nav';
import { authenticate, getUser } from './helpers'




const Login = (props) => {

  const [state, setState] = useState({
    name: '',
    passeword: ''
  })

  const { name, password } = state;

  useEffect(() => {
    getUser() && props.history.push('/') // if user enters '/login' manually now he'll be pushed in the home page because getUser() == true so he is already registred (logged in)
  }, [])


  const hendleChange = (n) => (e) => {
    console.log('name', n, 'event', e.target.value)
    setState({ ...state, [n]: e.target.value })
  }
  const { title, content, user } = state;


  const hendleSubmit = (e) => {
    e.preventDefault();
    // console.table({ name, password });
    axios.post(`${process.env.REACT_APP_API}/login`, { name, password })
      .then((response) => {
        console.log('login',response)
        // response will contain token and name of the user _> then we need to save them in the local storage 
        sessionStorage.setItem('token',JSON.stringify(response.data.token))
        sessionStorage.setItem('user',JSON.stringify(response.data.name))
        props.history.push('/create')
        // once loged user can can be redirected to the create page 
        // authenticate(response, () => props.history.push('/create')) 

      })
      .catch((error) => {
        alert('Invalid Password')
        console.log(error.response)
        //   alert(error.response.data.response)
      })
  }

  return (
    <div className='container pb-5'>
      <Nav />
      <br />
      <h1>Login 🔐</h1>
      <br />
      <form>
        <div className='from-group'>
          <label className='text-muted' >Name</label>

          <input onChange={hendleChange('name')} value={name} type='text' className='form-control' placeholder='Enter yourname' required />
        </div>

        <div className='from-group'>
          <label className='text-muted' >Password</label>

          <input onChange={hendleChange('password')} value={password} type='password' className='form-control' placeholder='Enter your password ' required />
        </div>
        <br />
        <div>
          <button  onClick={hendleSubmit}  className="buttton.btn.btn-primary">Login</button>
        </div>
      </form>
    </div>
  )
}
export default withRouter(Login); // withRouter helps to access the history when we wrap our component with it // now history is available in the props