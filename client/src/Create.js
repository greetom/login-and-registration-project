import React,{useState} from 'react';
import axios from 'axios' 
import Nav from './Nav';

const Create = () => {
  // // state
  const [state,setState] = useState({ 
    title :'', 
    content : '',
    user :'',

 })

 const hendleChange = (n) => (e) => {
        console.log('name',n,'event',e.target.value )
       setState({...state, [n] : e.target.value })
 }
    const {title,content,user} = state;
    

   const hendleSubmit = (e) =>{
    e.preventDefault()
    axios.post(`${process.env.REACT_APP_API}/post`,{title,content,user})
    .then((response) =>{
          setState({title:'',content:'',user:''})
          console.log(response)
          alert(`Post is created`)
    })
    .catch((error) =>{
      console.log('err',error)
      alert('error')
    })}

 return ( // we want to crontrol the values in our input fields based to what we have on the state , the updated state will be the value bellow 
 <div className='container p-5' >
      <Nav/>
      <br/>
      <h1>CREATE POST</h1>
      <br/>
      <form onSubmit={hendleSubmit}>    
              <div className='from-group'>
                  <label className='text-muted' >Title</label>
      
                  <input onChange={hendleChange('title')} value={title}  type='text' className='form-control'  placeholder='Give title' required/>
              </div>
              <div className='from-group'>
                  <label className='text-muted' >Content</label>
                  <textarea onChange={hendleChange('content')} value={content}   type='text' className='form-control'   placeholder='Write something...' required/>
              </div>
              <div className='from-group'>
                  <label className='text-muted' >User</label>
      
                  <input onChange={hendleChange('user')} value={user}  type='text' className='form-control'  placeholder='Enter user name' required/>
              </div>
              <br/>
              <div> 
                <button className="buttton.btn.btn-primary">Submit</button>
              </div>
      </form>
    </div>
  );
  

}
export default Create;
//