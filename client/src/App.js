import React, { useState, useEffect } from 'react'; // useEffect runs when the component mounts and unmounts
import Nav from './Nav';
import axios from 'axios' // to make get requests to the server
import {Link } from  'react-router-dom'

/*  now we need a state that'll hendle all the data that our component needs
then those data'll be displaye in our home page
*/

const App = () => {
  const [posts, setPosts] = useState([])


  const fetchPosts = async (req, res) => {
    const axio = await axios.get(`${process.env.REACT_APP_API}/getpost`)
    console.log('momo',axio)
    if (!axio) alert('Error Fetching posts')
    // return 
    console.log('All the data', axio)
    setPosts(axio.data) // quand tu regardes dans ta console t'as un objet data et dedans t'as l'ensemble de données sous post
    console.log(' data to post to the UI', axio.data)

  }





  // this fetchPost runs when the composent mounts and that means using useEffect AAAAAAND that is why we call fetchPost inside useEffect

  useEffect(() => { // first argument a function , second argument an empty array
    fetchPosts();

  }, [])


  // do not use {} when rendering data to the UI
  //elt.content.substring(0,100)
   


 return(
  <div className='container p-5' >
      <Nav />

      <br />

    <h1>MERN CRUD</h1>
    <br />
    {posts.map((elt,i) => (
            
    <div className="row" key={elt._id} style={{ borderBottom: '1px solid silver' }}>
      <div className="col pt-3 pb-2">

       <div className='row'>
          <div className='col-md-10'>
          <Link to={`/post/${elt.slug}`}>
          <h2>{elt.title}</h2>
                </Link>
              
              <p className="lead">{elt.content ? elt.content.substring(0,100) :''}</p>
              <p>
                Author <span className="badge">{elt.user}</span> Published on{' '}
                <span className="badge">{new Date(elt.createdAt).toLocaleString()}</span>
              </p>
          </div>
                 <div className='col-md-2'>

                    <Link to={`/post/update/${elt.slug}`} className='btn btn-sm btn-outline-warning' >
                    Update <span role="img">🔥</span>  
                    </Link>

                    <button className='btn btn-sm btn-outline-danger ml-1'>Delete <span role="img">😒</span></button>

                 </div>
       </div>
      </div>
    </div>
    ))}
  
  </div>
  )
};

export default App;

//____________________________________________________________________________________________  ________________________________________________________________________________


// import React, { useState, useEffect } from 'react';
// import Nav from './Nav';
// import axios from 'axios';
// import { Link } from 'react-router-dom';

// const App = (props) => {
//     const [post, setPosts] = useState([]);

//     const fetchPosts = () => {
//         axios
//             .get(`${process.env.REACT_APP_API}/getpost`)
//             .then(response => {
//                 // console.log(response);
//                 setPosts(response.data);
//             })
//             .catch(error => alert('Error fetching posts'));
//     };

//     useEffect(() => {
//         fetchPosts();
//     }, []);
    
      
// // console.log(props)
//     return (
//         <div className="container pb-5">
//             <Nav />
//             <br />
//             <h1>MERN CRUD</h1>
//             <hr />
//             {post.map((post,i) => (
//       <div className="row" key={post._id} style={{ borderBottom: '1px solid silver' }}>
//           <div className="col pt-3 pb-2">
//               <div className="row">
//                   <div className="col-md-10">
//                       <Link to={`/post/${post.slug}`}>
//                           <h2>{post.title}</h2>
//                       </Link>
//                       <p className="lead">{post.content.substring(0,100)}</p>
//                       <p>
//                           Author <span className="badge">{post.user}</span> Published on{''}
//                           <span className="badge">{new Date(post.createdAt).toLocaleString()}</span>
//                       </p>
//                   </div>

//                   <div className="col-md-2">
//                       <Link to={`/post/update/${post.slug}`} className="btn btn-sm btn-outline-warning">
//                           Update
//                       </Link>
//                       <button className="btn btn-sm btn-outline-danger ml-1">Delete</button>
//                   </div>
//               </div>
//           </div>
//       </div>
//   ))}
//         </div>
//     );
// };

// export default App;


