import React, { useState ,useEffect} from "react";
import axios from 'axios'
import Nav from './Nav' // pour naviguer à traver diff liens

/*  En React les attributs sont connus sous forme de “props” (abréviation de “properties”).

La plupart des composants peuvent être personnalisés avec différents props lors de leur création.

Les props d’un composant sont sous forme d’un objet qui contient des informations sur ce composant. Pour voir les props de ce dernier on utilise l’expression : this.props.

 */

const SinglePost = (props) =>{

 
    const ff = async () =>{
        const bob = await axios.get(`${process.env.REACT_APP_API}/post/${props.match.params.abdou}`) // reaching a specifique url [props.match.params.abdou] to post our information from the back there
        if(!bob)  alert('Error might happen from the back')
        console.log(bob)
        setPost(bob.data.ax)} // setting the value that a get from the server(DB)
        

    // return <div> {JSON.stringify(props)}</div>  // if u want to see informations about your component (SinglePost here) // 
    const [post,setPost] = useState('')
    useEffect( () =>{
        ff();

    }
    ,[])

    return(

        <div className='container p-5' >
        <Nav />
        <br />
        <h1>{post.title}</h1>
        <p className="lead">{post.content}</p>
        <p>
            Author <span className="badge">{post.user}</span> Published on{' '}
            <span className="badge">{new Date(post.createdAt).toLocaleString()}</span>
        </p>

       </div>     
    )




};
export default SinglePost;