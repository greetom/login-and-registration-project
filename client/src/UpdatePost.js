import React, { useState, useEffect } from "react";
import axios from 'axios'
import Nav from './Nav' // pour naviguer à traver diff liens
// import SinglePost from './SinglePost';




/*  En React les attributs sont connus sous forme de “props” (abréviation de “properties”).

La plupart des composants peuvent être personnalisés avec différents props lors de leur création.

Les props d’un composant sont sous forme d’un objet qui contient des informations sur ce composant. Pour voir les props de ce dernier on utilise l’expression : this.props.

 */

const UpdatePost = (props) => {
    const [state, setState] = useState({
        title: '',
        content: '',
        slug: '',
        user: ''
    })

    const { title, content, slug, user } = state


    const ff = async () => {
        const bob = await axios.get(`${process.env.REACT_APP_API}/post/${props.match.params.abdou}`) // reaching a specifique url [props.match.params.abdou] to post our information from the back there
        if (!bob) alert('Error might happen from the back')
        console.log('ici',bob)
        const { title, content, slug, user } = bob.data
        setState({ ...state, title, content, slug, user })
    } // setting the value that a get from the server(DB)

    useEffect(() => {
        ff();
    }, [])
    
    const hendleChange = (n) => (e) => {  // n pour name je crois   
        // console.log('name', n, 'event', e.target.value)
        setState({ ...state, [n]: e.target.val })
    }

  /* n _> clé : e _> val */
    const hendleSubmit =  (e) => {
        e.preventDefault();
          axios.put(`${process.env.REACT_APP_API}/post/${slug}`, {title,content,user}) // second argument : the updated constent
                .then((res)=>{
                    console.log('taaw',res)
                    const { title, content, slug, user} = res.data
                    setState({...state,title,content,slug,user})
                    alert('Post is created ! ')
                .catch((err)=>{
                    console.log(err)
                    alert('err')
                })
                })

    }






    const showUpdateForm = () => (
          <form onSubmit={hendleSubmit}>
    <div className='from-group'>
        <label className='text-muted' >Title</label>

        <input onChange={hendleChange('title')} value={title} type='text' className='form-control' placeholder='Give title' required />
    </div>
    <div className='from-group'>
        <label className='text-muted' >Content</label>
        <textarea onChange={hendleChange('content')} value={content} type='text' className='form-control' placeholder='Write something...' required />
    </div>
    <div className='from-group'>
        <label className='text-muted' >User</label>
        <input onChange={hendleChange('user')} value={user} type='text' className='form-control' placeholder='Enter user name' required />
    </div>
    <br />
    <div>
        <button className="buttton.btn.btn-primary">Update</button>
    </div>
</form>)
    return (



        <div className='container p-5' >
            <Nav />
            <br />
            <h1>UDPADE POST</h1>
            {showUpdateForm()}
        </div>
    )




};

export default UpdatePost;

